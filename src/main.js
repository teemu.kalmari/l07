// initialize
console.log('Program Starting')
const router = require('./router')
const PORT = 3000;
const HOST = "127.0.0.1"; //0.0.0.0


// operate
router.listen(PORT,HOST, () => console.log(`Listening to http://${HOST}:${PORT}`))

// cleanup
console.log('program ending');