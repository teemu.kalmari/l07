const expect = require("chai").expect;
const router = require("../src/router");
const PORT = 3000;
const HOST = "127.0.0.1"
const baseurl = `http://${HOST}:${PORT}`;

describe("Test router", () => {
    let server;
    before("Start server", (done) => {
        server = router.listen(
            PORT,
            HOST,
            () => done()
        );
    });
    describe("Routes", () => {
        it("Can GET welcome message", async() => {
            const response = await fetch(baseurl);
            const response_msg = await response.text();
            expect(response_msg).to.equal("welcome!");

        });
        it("Can get sum of two numbers", async () => {
            const endpoint = baseurl + "/add";
            const getResponse = (a, b) => new Promise(async ( resolve, reject) => {
                const query =`${endpoint}?a=${a}&b=${b}`;
                const response = await(await fetch(query)).text();
                resolve(response);
            });
           
            console.log(await getResponse(1, 2));
            expect(await getResponse(1,2)).to.equal("3")
            

        });
    })
});
